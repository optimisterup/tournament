Tournament (Symfony 5.0)
========================

Installing project
------------------

1. go to work dir
    ```bash
    cd /path/to/workdir
    ```
2. clone project
    ```bash
    git clone git@bitbucket.org:optimisterup/tournament.git
    ```
3. go to project dir
    ```bash
    cd /path/to/project
    ```
4. [install composer globally or locally in project folder](http://symfony.com/doc/current/setup/composer.html)

5. composer install
   ```bash
    composer install
   ```

6. Create MySQL database
   ```bash
   bin/console doctrine:database:create
   bin/console doctrine:migrations:migrate
   ```

7. Install Assets
   ```bash
   bin/console assets:install --symlink
   ```
<br />

Enjoy!
