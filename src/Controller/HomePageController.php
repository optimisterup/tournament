<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Enum\GameResultEnum;
use App\Repository\Result\AbstractResultRepository;
use App\Repository\Result\QualificationResultRepository;
use App\Service\FixtureService;
use App\Repository\DivisionRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(
        DivisionRepository $divisionRepository,
        QualificationResultRepository $qualificationResultRepository,
        AbstractResultRepository $abstractResultRepository): Response
    {
        $divisions                = $divisionRepository->findAll();
        $qualificationGameResults = $qualificationResultRepository->getAllResults();
        $playoffGameResults       = $abstractResultRepository->getAllResults();
        $topTeamList              = $abstractResultRepository->getTopTeamsOrderedByScore();

        return $this->render('home/index.html.twig', [
            'divisions'                => $divisions,
            'qualificationGameResults' => $qualificationGameResults,
            'playoffGameResults'       => $playoffGameResults,
            'playoffResultTypes'       => GameResultEnum::getPlayOffTypes(),
            'topTeamList'              => $topTeamList,
        ]);
    }

    /**
     * @Route("/fixtures", name="fixtures_load")
     * */
    public function fixturesLoad(FixtureService $fixtureService): RedirectResponse
    {
        $fixtureService->load();

        return $this->redirectToRoute('index');
    }
}
