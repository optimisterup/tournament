<?php
declare(strict_types = 1);

namespace App\DTO;

class GameDTO
{
    /**
     * @var string
     */
    private $teamName;

    /**
     * @var integer
     */
    private $teamId;

    /**
     * @var string
     */
    private $score;

    /**
     * @param string      $teamName
     * @param int         $teamId
     * @param string      $score
     */
    public function __construct(
        string $teamName,
        int    $teamId,
        string $score
    ) {
        $this->teamName      = $teamName;
        $this->teamId        = $teamId;
        $this->score         = $score;
    }

    /**
     * @return string
     */
    public function getScore(): string
    {
        return $this->score;
    }

    /**
     * @return integer
     */
    public function getTeamId(): int
    {
        return $this->teamId;
    }

    /**
     * @return string
     */
    public function getTeamName(): string
    {
        return $this->teamName;
    }
}
