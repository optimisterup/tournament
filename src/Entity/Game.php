<?php
declare(strict_types = 1);

namespace App\Entity;

use App\Entity\Result\AbstractResult;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Game extends BaseEntity
{
    /**
     * @ORM\OneToMany(targetEntity=AbstractResult::class, mappedBy="game")
     */
    private $results;


    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    /**
     * @return Collection|AbstractResult[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    /**
     * @param AbstractResult $result
     * @return $this
     */
    public function addResult(AbstractResult $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
        }

        return $this;
    }

    /**
     * @param AbstractResult $result
     * @return $this
     */
    public function removeResult(AbstractResult $result): self
    {
        $this->results->removeElement($result);

        return $this;
    }
}
