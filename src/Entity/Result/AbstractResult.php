<?php
declare(strict_types = 1);

namespace App\Entity\Result;

use App\Entity\BaseEntity;
use App\Entity\Team;
use App\Entity\Game;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="result")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="game_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "qualification" = "App\Entity\Result\QualificationResult",
 *     "playoff"       = "App\Entity\Result\PlayOffResult",
 *     "semifinal"     = "App\Entity\Result\SemiFinalResult",
 *     "final"         = "App\Entity\Result\FinalResult",
 * })
 */
abstract class AbstractResult extends BaseEntity
{
    public const MIN_SCORE = 0;
    public const MAX_SCORE = 1;

    public const QUALIFICATION = 'qualification';
    public const PLAY_OFF      = 'playoff';
    public const SEMIFINAL     = 'semifinal';
    public const FINAL         = 'final';

    abstract function getType(): string;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, inversedBy="results")
     */
    private $team;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="results")
     */
    private $game;

    /**
     * @param Game $game
     * @param Team         $team
     * @param int          $score
     */
    public function __construct(Game $game, Team $team, int $score=0)
    {
        $this->game  = $game;
        $this->team  = $team;
        $this->score = $score;
    }

    /**
     * @return Team|null
     */
    public function getTeam(): ?Team
    {
        return $this->team;
    }

    /**
     * @return int|null
     */
    public function getScore(): ?int
    {
        return $this->score;
    }

    /**
     * @return Game|null
     */
    public function getGame(): ?Game
    {
        return $this->game;
    }

    /**
     * @return integer
     */
    public function getWeight(): int
    {
        return $this->weight;
    }
}
