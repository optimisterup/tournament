<?php
declare(strict_types = 1);

namespace App\Entity\Result;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\Result\FinalResultRepository;

/**
 * @ORM\Entity(repositoryClass=FinalResultRepository::class)
 */
class FinalResult extends AbstractResult
{
    /**
     * @return string
     */
    function getType(): string
    {
        return static::FINAL;
    }
}
