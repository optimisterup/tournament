<?php
declare(strict_types = 1);

namespace App\Entity\Result;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\Result\PlayOffResultRepository;

/**
 * @ORM\Entity(repositoryClass=PlayOffResultRepository::class)
 */
class PlayOffResult extends AbstractResult
{

    /**
     * @return string
     */
    function getType(): string
    {
        return static::PLAY_OFF;
    }
}
