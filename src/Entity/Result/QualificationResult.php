<?php
declare(strict_types = 1);

namespace App\Entity\Result;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\Result\QualificationResultRepository;

/**
 * @ORM\Entity(repositoryClass=QualificationResultRepository::class)
 */
class QualificationResult extends AbstractResult
{

    /**
     * @return string
     */
    function getType(): string
    {
        return static::QUALIFICATION;
    }
}
