<?php
declare(strict_types = 1);

namespace App\Entity\Result;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\Result\SemiFinalResultRepository;

/**
 * @ORM\Entity(repositoryClass=SemiFinalResultRepository::class)
 */
class SemiFinalResult extends AbstractResult
{

    /**
     * @return string
     */
    function getType(): string
    {
        return static::SEMIFINAL;
    }
}
