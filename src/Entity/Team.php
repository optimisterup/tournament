<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TeamRepository;
use App\Entity\Result\AbstractResult;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=TeamRepository::class)
 * @UniqueEntity("name")
 */
class Team extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity=Division::class, inversedBy="teams")
     */
    private $division;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=AbstractResult::class, mappedBy="team")
     */
    private $results;

    /**
     * @param string $name
     * @param Division $division
     */
    public function __construct(string $name, Division $division)
    {
        $this->name = $name;
        $this->division = $division;
        $this->results = new ArrayCollection();
    }

    /**
     * @return Division|null
     */
    public function getDivision(): ?Division
    {
        return $this->division;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return Collection|AbstractResult[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    /**
     * @param AbstractResult $result
     * @return $this
     */
    public function addResult(AbstractResult $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
        }

        return $this;
    }

    /**
     * @param AbstractResult $result
     * @return $this
     */
    public function removeResult(AbstractResult $result): self
    {
        $this->results->removeElement($result);

        return $this;
    }
}
