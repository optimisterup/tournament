<?php
declare(strict_types = 1);

namespace App\Enum;


class DivisionEnum
{
    public const DIVISION_NAME_A = 'A';
    public const DIVISION_NAME_B = 'B';

    public static function getDivisionNames(): array
    {
        return [
            static::DIVISION_NAME_A,
            static::DIVISION_NAME_B,
        ];
    }

    /**
     * @return array
     */
    public static function getDivisionTeamNames(): array
    {
        return [
            static::DIVISION_NAME_A => range('A', 'H'),
            static::DIVISION_NAME_B => range('I', 'P'),
        ];
    }
}
