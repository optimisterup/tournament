<?php
declare(strict_types = 1);

namespace App\Enum;


use App\Entity\Result\AbstractResult;


class GameResultEnum
{
    public static function getPlayOffTypes(): array
    {
        return [
            AbstractResult::PLAY_OFF,
            AbstractResult::SEMIFINAL,
            AbstractResult::FINAL,
        ];
    }
}
