<?php
declare(strict_types = 1);

namespace App\Factory;


use App\Entity\Division;

class DivisionFactory
{
    /**
     * @param string $divisionName
     * @return Division
     */
    public static function create(string $divisionName): Division
    {
        return new Division($divisionName);
    }
}
