<?php
declare(strict_types = 1);

namespace App\Factory;


use App\Entity\Game;

class GameFactory
{
    /**
     * @return Game
     */
    public static function create(): Game
    {
        return new Game();
    }
}
