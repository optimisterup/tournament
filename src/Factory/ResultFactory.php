<?php
declare(strict_types = 1);

namespace App\Factory;

use App\Entity\Game;
use App\Entity\Team;
use App\Entity\Result\FinalResult;
use App\Entity\Result\PlayOffResult;
use App\Entity\Result\SemiFinalResult;
use App\Entity\Result\QualificationResult;


class ResultFactory
{
    /**
     * @param Game $game
     * @param Team $team
     * @param int $score
     * @return QualificationResult
     */
    public static function createQualificationResult(Game $game, Team $team, int $score = 0): QualificationResult
    {
        return new QualificationResult($game, $team, $score);
    }

    /**
     * @param Game $game
     * @param Team $team
     * @param int $score
     * @return PlayOffResult
     */
    public static function createPlayoffResult(Game $game, Team $team, int $score = 0): PlayOffResult
    {
        return new PlayOffResult($game, $team, $score);
    }


    /**
     * @param Game $game
     * @param Team $team
     * @param int $score
     * @return SemiFinalResult
     */
    public static function createSemifinalResult(Game $game, Team $team, int $score = 0): SemiFinalResult
    {
        return new SemiFinalResult($game, $team, $score);
    }

    /**
     * @param Game $game
     * @param Team $team
     * @param int $score
     * @return FinalResult
     */
    public static function createFinalResult(Game $game, Team $team, int $score = 0): FinalResult
    {
        return new FinalResult($game, $team, $score);
    }

}
