<?php
declare(strict_types = 1);

namespace App\Factory;


use App\Entity\Team;
use App\Entity\Division;

class TeamFactory
{
    /**
     * @param string   $teamName
     * @param Division $division
     *
     * @return Team
     */
    public static function create(string $teamName, Division $division) : Team
    {
        return new Team($teamName, $division);
    }
    
}
