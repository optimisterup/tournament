<?php
declare(strict_types = 1);

namespace App\Hydrator;


use Doctrine\ORM\Internal\Hydration\ObjectHydrator;

abstract class AbstractGameDataHydrator extends ObjectHydrator
{
    /**
     * @var array
     */
    protected $totalScoreByTeam =[];

    /**
     * @param array $games
     * @return array
     */
    protected function parseGameData(array $games): array
    {
        $results = [];
        foreach ($games as $game) {
            $teamId = $game['teamId'];
            $gameId = $game['gameId'];
            $gameScore = $game['score'];
            $resultType = $game['type'];

            $results[$resultType][$gameId]['score'][$teamId] = $gameScore;
            $results[$resultType][$gameId]['teams'][$teamId] = $game['teamName'];
            $results[$resultType][$gameId]['division'][$teamId] = $game['divisionName'];
            $this->totalScoreByTeam[$resultType][$teamId] =
                isset($this->totalScoreByTeam[$resultType][$teamId]) ?
                    ($this->totalScoreByTeam[$resultType][$teamId] + $gameScore) :
                    $gameScore;
        }

        return $results;
    }

    /**
     * @param array $scores
     * @return string
     */
    protected function implodeTeamScore(array $scores): string
    {
        return implode(':', $scores);
    }
}
