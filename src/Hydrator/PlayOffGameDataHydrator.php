<?php
declare(strict_types = 1);

namespace App\Hydrator;


use App\DTO\GameDTO;
use Doctrine\ORM\Internal\Hydration\ObjectHydrator;


class PlayOffGameDataHydrator extends AbstractGameDataHydrator
{

    /**
     * @return array
     */
    protected function hydrateAllData(): array
    {
        $games = parent::hydrateAllData();
        $tableResults = [];
        if (count($games) > 0) {
            $gameResultsByType = $this->parseGameData($games);
            foreach ($gameResultsByType as $resultType => $results) {
                foreach ($results as $gameId => $gameResult) {
                    $gameScore = implode(':', $gameResult['score']);;
                    $tableResults[$resultType]['games'][$gameId] = $gameResult['teams'];
                    $tableResults[$resultType]['scores'][$gameId] = $gameScore;;
                }
            }
            return $tableResults;
        }
        return [];
    }
}
