<?php
declare(strict_types = 1);

namespace App\Hydrator;

use App\DTO\GameDTO;


class QualificationGameDataHydrator extends AbstractGameDataHydrator
{

    /**
     * @return array
     */
    protected function hydrateAllData(): array
    {
        $games = parent::hydrateAllData();
        if (count($games) > 0) {
            $gameResultsByType = $this->parseGameData($games);
            foreach ($gameResultsByType as $results) {
                foreach ($results as $gameResult) {
                    $gameScore = $this->implodeTeamScore($gameResult['score']);
                    $gameResults[array_key_first(($gameResult['teams']))] = $gameScore;
                    $gameResults[array_key_last(($gameResult['teams']))] = strrev($gameScore);

                    foreach ($gameResult['teams'] as $teamId => $teamName) {
                        $tableResults[$teamId][] = new GameDTO(
                            $teamName,
                            $teamId,
                            $gameResults[$teamId]
                        );
                    }
                }
            }

            $tableResults['totalScoreByTeam'] = $this->totalScoreByTeam;
            return $tableResults;
        }

        return [];
    }
}
