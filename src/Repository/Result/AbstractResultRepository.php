<?php

namespace App\Repository\Result;

use App\Entity\Team;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use App\Entity\Result\AbstractResult;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, $className = null)
    {
        parent::__construct($registry, $className ?? AbstractResult::class);
    }

     /**
      * @return AbstractResult[] Returns an array of Team objects
      */
    public function getAllResults(): array
    {
        return $this->getAllResultsQuery()
            ->getResult('playoff_game_data_hydrator');
    }

    /**
     * @param string|null $divisionName
     * @param integer|null $limit
     * @return array
     */
    public function getTopTeamsOrderedByScore(string $divisionName = null, int $limit = null): array
    {
        $qb = $this->createQueryBuilder('r')
            ->select(
                'team.id as teamId',
                'team.name as teamName',
                'SUM(r.score) as teamScore'
            )
            ->join('r.team', 'team')
            ->join('team.division', 'division')
            // best teams have more games
            ->addOrderBy('COUNT(r.id)', 'DESC')
            ->addOrderBy('teamScore', 'DESC')
            ->groupBy('team.id');

        if (null !== $divisionName) {
            $qb->where('division.name = :divisionName')
               ->setParameter('divisionName', $divisionName);
        }
        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @return Query
     */
    protected function getAllResultsQuery(): Query
    {
        $queryBuilder = $this->createQueryBuilder('r');
        return static::selectResultFields($queryBuilder)
            ->join('r.team', 'team')
            ->join('r.game', 'game')
            ->join('team.division', 'division')
            ->orderBy('team.name', 'ASC')
            ->addOrderBy('game.id', 'ASC')
            ->getQuery();
    }


    /**
     * @param QueryBuilder $queryBuilder A QueryBuilder instance.
     *
     * @return QueryBuilder
     */
    protected static function selectResultFields(QueryBuilder $queryBuilder): QueryBuilder
    {
        return $queryBuilder->select(
            'team.name as teamName',
            'team.id as teamId',
            'game.id as gameId',
            'r.score as score',
            'division.name as divisionName',
            "CASE
                    WHEN r INSTANCE OF App\Entity\Result\PlayOffResult   THEN '".AbstractResult::PLAY_OFF."'
                    WHEN r INSTANCE OF App\Entity\Result\SemiFinalResult THEN '".AbstractResult::SEMIFINAL."'
                    WHEN r INSTANCE OF App\Entity\Result\FinalResult     THEN '".AbstractResult::FINAL."'
                ELSE '".AbstractResult::QUALIFICATION."' END AS type"
        );
    }
}
