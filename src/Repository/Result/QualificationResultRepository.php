<?php

namespace App\Repository\Result;

use App\Entity\Team;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Result\QualificationResult;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualificationResultRepository extends AbstractResultRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QualificationResult::class);
    }

    /**
     * @return array
     */
    public function getAllResults(): array
    {
        return parent::getAllResultsQuery()
        ->getResult('qualification_game_data_hydrator');
    }
}
