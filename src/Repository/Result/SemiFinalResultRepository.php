<?php

namespace App\Repository\Result;

use App\Entity\Team;
use App\Entity\Result\SemiFinalResult;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SemiFinalResultRepository extends AbstractResultRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SemiFinalResult::class);
    }
}
