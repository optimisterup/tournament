<?php
declare(strict_types = 1);

namespace App\Service;

use App\Entity\Division;
use App\Entity\Result\AbstractResult;
use App\Entity\Result\QualificationResult;
use App\Entity\Team;
use App\Factory\DivisionFactory;
use App\Factory\GameFactory;
use App\Factory\ResultFactory;
use App\Factory\TeamFactory;
use App\Enum\DivisionEnum;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FixtureService
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em) {
        $this->em= $em;
    }


    /**
     * @throws Exception
     */
    public function load()
    {
        $divisionExistedCount = $this->em->getRepository(Division::class)->getCount();

        if (0 === $divisionExistedCount) {
            $divisionTeamNames = DivisionEnum::getDivisionTeamNames();
            $teams = $this->createDivisionTeams($divisionTeamNames);
            $divisionTeamGames = static::getDivisionTeamGames($divisionTeamNames);
            // Qualification Game
            $this->generateQualificationGameResults($teams, $divisionTeamGames);
            $this->em->flush();

            $topTeams = [];
            foreach (DivisionEnum::getDivisionNames() as $divisionName) {
                $divisionTeamCount = count(DivisionEnum::getDivisionTeamNames()[$divisionName]);
                $topTeamCount = intdiv($divisionTeamCount, 2);
                $topTeams[$divisionName] = $this->em
                    ->getRepository(QualificationResult::class)
                    ->getTopTeamsOrderedByScore($divisionName, $topTeamCount);
            }

            // Each top team will be play with low team from other division
            $reversedTeamDivisionB = array_reverse($topTeams[DivisionEnum::DIVISION_NAME_B]);

            $playoffGames = [];
            foreach ($topTeams[DivisionEnum::DIVISION_NAME_A] as $topTeamNumber => $teamDivisionA) {
                $playoffGames[] = [$teamDivisionA['teamId'], $reversedTeamDivisionB[$topTeamNumber]['teamId']];
            }

            $semiFinalTeamIds = $this->generatePlayOffGameResults($playoffGames);

            $finalTeamIds = $this->generatePlayOffGameResults(
                static::generatePlayOffGames($semiFinalTeamIds),
                AbstractResult::SEMIFINAL
            );

            $this->generatePlayOffGameResults(
                static::generatePlayOffGames($finalTeamIds),
                AbstractResult::FINAL
            );

            $this->em->flush();
        }
    }

    /**
     * @param array $divisionTeamNames
     * @return array
     */
    private function createDivisionTeams(array $divisionTeamNames): array
    {
        $teams = [];
        foreach (DivisionEnum::getDivisionNames() as $divisionName) {
            $division = DivisionFactory::create($divisionName);
            foreach ($divisionTeamNames[$divisionName] as $teamName) {
                $this->em->persist($teams[$divisionName][$teamName] = TeamFactory::create($teamName, $division));
            }
            $this->em->persist($division);
        }

        return $teams;
    }

    /**
     * @param array $divisionTeamNames
     * @return array
     */
    private static function getDivisionTeamGames(array $divisionTeamNames): array
    {
        $plays = [];
        foreach ($divisionTeamNames as $divisionName => $divisionTeams) {
            $guestTeams = $divisionTeams;
            $plays[$divisionName] = [];
            foreach ($divisionTeams as $key => $hostTeam) {
                /* Unset host team for avoid duplicates */
                unset($guestTeams[$key]);

                foreach ($guestTeams as $guestTeam) {
                    $plays[$divisionName][] = [
                        $hostTeam,
                        $guestTeam,
                    ];
                }
            }
        }

        return $plays;
    }

    /**
     * @param array $teams
     * @param array $divisionTeamGames
     *
     * @return void
     * @throws Exception
     */
    private function generateQualificationGameResults(array $teams, array $divisionTeamGames): void
    {
        foreach ($divisionTeamGames as $divisionName => $divisionGames) {
            foreach ($divisionGames as $divisionGame) {
                $game = GameFactory::create();
                $this->em->persist($game);
                $teamResults = static::generateRandomGameScores();
                foreach ($divisionGame as $numberTeam => $teamName) {
                    $result = ResultFactory::createQualificationResult($game, $teams[$divisionName][$teamName], $teamResults[$numberTeam]);
                    $this->em->persist($result);
                }
            }
        }
    }

    /**
     * @param array $playOffTeamGames
     * @param string $resultType
     *
     * @return array
     *
     * @throws ORMException
     * @throws Exception
     */
    private function generatePlayOffGameResults(array $playOffTeamGames, string $resultType = AbstractResult::PLAY_OFF): array
    {
        $winnerTeamIds = [];
        foreach ($playOffTeamGames as $playOffTeamGame) {
            $game = GameFactory::create();
            $this->em->persist($game);
            $teamResults = static::generateRandomGameScores();
            foreach ($playOffTeamGame as $numberTeam => $teamId) {
                /** @var Team $team */
                $team = $this->em->getReference(Team::class, $teamId);
                $methodName = 'create'.lcfirst($resultType).'Result';
                $result = ResultFactory::$methodName($game, $team, $score = $teamResults[$numberTeam]);
                if (AbstractResult::MAX_SCORE == $score) {
                    $winnerTeamIds[]= $teamId;
                }
                $this->em->persist($result);
            }
        }

        return $winnerTeamIds;
    }

    /**
     * @param array $teamIds
     * @return array
     */
    private static function generatePlayOffGames(array $teamIds): array
    {
        return array_chunk($teamIds, 2);
    }



    /**
     * @return array
     * @throws Exception
     */
    private static function generateRandomGameScores(): array
    {
        $randomInt = random_int(AbstractResult::MIN_SCORE, AbstractResult::MAX_SCORE);
        if (AbstractResult::MAX_SCORE == $randomInt) {
            return [AbstractResult::MAX_SCORE, AbstractResult::MIN_SCORE];
        }
        return [AbstractResult::MIN_SCORE, AbstractResult::MAX_SCORE];
    }

}
