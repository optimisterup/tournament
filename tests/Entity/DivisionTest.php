<?php
declare(strict_types = 1);

namespace App\Tests\Entity;


use App\Entity\Division;
use App\Entity\Team;
use App\Enum\DivisionEnum;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class DivisionTest extends TestCase
{
    /**
     * @var Division
     */
    private $division;

    public function setUp(): void
    {
        $this->division = new Division(DivisionEnum::DIVISION_NAME_A);
        parent::setUp();
    }

    /**
     * @test
     */
    public function create()
    {
        $this->assertInstanceOf(Collection::class, $this->division->getTeams());
    }

    /**
     * @test
     */
    public function testAdd()
    {
        $teamNames = DivisionEnum::getDivisionTeamNames()[DivisionEnum::DIVISION_NAME_A];
        $teamCountInDivision = count($teamNames);
        foreach ($teamNames as $teamName) {
            $team = new Team($teamName, $this->division);
            $this->division->addTeam($team);
        }

        $this->assertEquals($teamCountInDivision, $this->division->getTeams()->count());
    }

    /**
     * @test
     */
    public function testRemove()
    {

        $teamName = DivisionEnum::getDivisionTeamNames()[DivisionEnum::DIVISION_NAME_A][0];
        $team = new Team($teamName, $this->division);
        $this->division->addTeam($team);
        $this->division->removeTeam($team);
        $this->assertEquals(0, $this->division->getTeams()->count());
    }


}
