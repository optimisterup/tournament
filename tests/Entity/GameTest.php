<?php
declare(strict_types = 1);

namespace App\Tests\Entity;


use App\Entity\Division;
use App\Entity\Game;
use App\Entity\Result\AbstractResult;
use App\Entity\Result\QualificationResult;
use App\Entity\Team;
use App\Enum\DivisionEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    /**
     * @var Game
     */
    private $game;

    public function setUp(): void
    {
        $this->game = new Game();
        parent::setUp();
    }

    /**
     * @test
     */
    public function create()
    {
        $this->assertInstanceOf(ArrayCollection::class, $this->game->getResults());
    }

    /**
     * @test
     */
    public function addResult()
    {
        $result = new QualificationResult($this->game, new Team('A', new Division('F')), 0);
        $this->game->addResult($result);
        $this->assertEquals(1, $this->game->getResults()->count());
    }

    /**
     * @test
     */
    public function removeResult()
    {
        $result = new QualificationResult($this->game, new Team('A', new Division('F')), 0);
        $this->game->addResult($result);
        $this->game->removeResult($result);

        $this->assertEquals(0, $this->game->getResults()->count());
    }
}
