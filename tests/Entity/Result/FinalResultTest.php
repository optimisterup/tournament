<?php
declare(strict_types = 1);

namespace App\Tests\Entity\Result;


use App\Entity\Division;
use App\Entity\Game;
use App\Entity\Result\AbstractResult;
use App\Entity\Result\FinalResult;
use App\Entity\Team;
use App\Enum\DivisionEnum;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class FinalResultTest extends TestCase
{
    /**
     * @var FinalResult
     */
    private $result;

    public function setUp(): void
    {
        $this->result = new FinalResult(
            new Game(),
            new Team('A', new Division('E')),
            1
        );
        parent::setUp();
    }

    /**
     * @test
     */
    public function create()
    {
        $this->assertEquals(1, $this->result->getTeam());
    }

    /**
     * @test
     */
    public function testType()
    {
        $this->assertEquals(FinalResult::FINAL, $this->result->getType());
    }
}
