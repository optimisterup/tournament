<?php
declare(strict_types = 1);

namespace App\Tests\Entity;


use App\Entity\Division;
use App\Entity\Game;
use App\Entity\Result\AbstractResult;
use App\Entity\Result\QualificationResult;
use App\Entity\Team;
use App\Enum\DivisionEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;

class TeamTest extends TestCase
{
    /**
     * @var Division
     */
    private $team;

    public function setUp(): void
    {
        $this->team = new Team('Team A', new Division(DivisionEnum::DIVISION_NAME_A));
        parent::setUp();
    }

    /**
     * @test
     */
    public function create()
    {
        $this->assertInstanceOf(ArrayCollection::class, $this->team->getResults());
    }

    /**
     * @test
     */
    public function addResult()
    {
        $game = new Game();
        $this->team->addResult(new QualificationResult($game, $this->team));
        $this->assertEquals(1, $this->team->getResults()->count());
    }

    /**
     * @test
     */
    public function removeResult()
    {
        $game = new Game();
        $result = new QualificationResult($game, $this->team);
        $this->team->addResult($result);
        $this->team->removeResult($result);
        $this->assertEquals(0, $this->team->getResults()->count());
    }
}
